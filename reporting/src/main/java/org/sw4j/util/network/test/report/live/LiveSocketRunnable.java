/*
 * Copyright (C) 2017 Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.sw4j.util.network.test.report.live;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author Uwe Plonus &lt;u.plonus@gmail.com&gt;
 */
public class LiveSocketRunnable extends LiveDataRunnable {

    private final Socket socket;

    private final String title;

    public LiveSocketRunnable(Socket socket) throws IOException, XMLStreamException {
        super(socket.getInputStream());
        this.socket = socket;
        this.title = new StringBuilder(this.socket.getInetAddress().getHostName())
                .append(":").append(this.socket.getPort()).toString();
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }

    @Override
    public void closeConnection() throws IOException {
        this.socket.close();
    }

}
